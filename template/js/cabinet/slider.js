$(document).ready(function () {

    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 300,
        itemMargin: 5,
        minItems: 2,
        maxItems: 4
    });
});