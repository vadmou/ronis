$(document).ready(function () {

    $("form").validate({
        rules: {
            title: {
                required: true
            },
            position: {
                required: true,
                number: true,
                min: [1]
            },
            image: {
                required: true
            }
        },

        messages: {
            title: {
                required: "Это поле необходимо заполнить"
            },
            position: {
                required: "Укажите позицию в списке",
                number: "Значение должно быть цифровым",
                min: "Значение должно быть больше {0}"
            },
            image: {
                required: "Необходимо добавить изображение"
            }
        }
    });


    $(document).on('click', '.del', function () {
        var $imageId = $(this).val();
        var $userId = userId;
        var $src = $(this).prev().children().attr("src");
        var $src = "../../" + $src;
        $.ajax({
            url: "/template/ajax/main.php",
            method: 'post',
            dataType: 'json',
            data: {
                userId: $userId,
                imageId: $imageId,
                imageUrl: $src,
                flag: "1"
            },
            success: function (data) {
                var str = "";
                for (var id in data) {
                    str += "<div><p>Название: " + data[id]["title"] + "</p>\n" +
                        "Позиция в списке: <input id=\"position\" type=\"text\" name=\"position\" value=\""
                        + data[id]["position"] + "\" />" +
                        "<button  name=\"button\" class=\"changePosition\" value=\"" + data[id]["id"] +
                        "\">Изменить</button><br>" +
                        "<a href=\"" + data[id]["link"] + "\" >" +
                        "<img class=\"del\" src=\"" + data[id]["url"] + "\"></a>" +
                        "<button class=\"del btn btn-danger\" name=\"button\" value=\"" + data[id]["id"] +
                        "\">Удалить</button>\n<hr></div>";
                }

                $("#notes").empty().append(str);
            },
            error: function () {
                console.log("error");
            }
        })
    });

    $(document).on('click', '.changePosition', function () {
        var $imageId = $(this).val();
        var $position = $(this).prev('input').val();
        var $userId = userId;
        $.ajax({
            url: "/template/ajax/main.php",
            method: 'post',
            dataType: 'json',
            data: {
                userId: $userId,
                position: $position,
                imageId: $imageId,
                flag: "2"
            },
            success: function (data) {
                var str = "";
                for (var id in data) {
                    str += "<div><p>Название: " + data[id]["title"] + "</p>\n" +
                        "Позиция в списке: <input id=\"position\" type=\"text\" name=\"position\" value=\"" +
                        data[id]["position"] + "\" />" +
                        "<button name=\"button\" class=\"changePosition\" value=\"" + data[id]["id"] +
                        "\">Изменить</button><br>" +
                        "<a href=\"" + data[id]["link"] + "\" >" +
                        "<img class=\"del\" src=\"" + data[id]["url"] + "\"></a>" +
                        "<button class=\"del btn btn-danger\" name=\"button\" value=\"" + data[id]["id"] +
                        "\">Удалить</button>\n<hr></div>";
                }

                $("#notes").empty().append(str);
            },
            error: function () {
                console.log("error");
            }
        })
    });
});