<?php
require_once('../../components/Db.php');
$db = Db::getConnection();

if (isset($_POST)) {
    if ($_POST['flag'] == "1") {
        $id = $_POST['imageId'];
        $user_id = htmlspecialchars($_POST['userId'], ENT_QUOTES);
        $url = $_POST['imageUrl'];


        if (file_exists($url)) {
            unlink($url);
        }


        $result = $db->prepare('DELETE FROM image WHERE id = :id');
        $result->bindValue(':id', $id, PDO::PARAM_STR);
        $result->execute();

        $Images = array();

        $result = $db->query('SELECT id, title, url, user_id, link, position FROM image' .
            ' WHERE user_id=' . $user_id);

        $i = 0;
        while ($row = $result->fetch()) {
            $Images[$i]['id'] = $row['id'];
            $Images[$i]['title'] = $row['title'];
            $Images[$i]['url'] = $row['url'];
            $Images[$i]['user_id'] = $row['user_id'];
            $Images[$i]['link'] = $row['link'];
            $Images[$i]['position'] = $row['position'];
            $i++;
        }

        print(json_encode($Images));
    }

    if ($_POST['flag'] == "2") {
        $id = htmlspecialchars($_POST['imageId'], ENT_QUOTES);
        $position = htmlspecialchars($_POST['position'], ENT_QUOTES);
        $user_id = htmlspecialchars($_POST['userId'], ENT_QUOTES);

        $result = $db->prepare('UPDATE image SET position = :position WHERE id = :id');
        $result->bindValue(':id', $id, PDO::PARAM_INT);
        $result->bindValue(':position', $position, PDO::PARAM_INT);
        $result->execute();

        $Images = array();

        $result = $db->query('SELECT id, title, url, user_id, link, position FROM image' .
            ' WHERE user_id=' . $user_id);

        $i = 0;
        while ($row = $result->fetch()) {
            $Images[$i]['id'] = $row['id'];
            $Images[$i]['title'] = $row['title'];
            $Images[$i]['url'] = $row['url'];
            $Images[$i]['user_id'] = $row['user_id'];
            $Images[$i]['link'] = $row['link'];
            $Images[$i]['position'] = $row['position'];
            $i++;
        }

        print(json_encode($Images));
    }
}
