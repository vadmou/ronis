<?php

class User
{
    /**
     * Enter data into database
     * @param $name
     * @param $email
     * @param $password
     * @return bool
     */

    public static function register($name, $email, $password)
    {

        $db = Db::getConnection();

        $sql = 'INSERT INTO user (name, email, password) '
                . 'VALUES (:name, :email, :password)';

        $result = $db->prepare($sql);
        $result->bindValue(':name', $name, PDO::PARAM_STR);
        $result->bindValue(':email', $email, PDO::PARAM_STR);
        $result->bindValue(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Check user in database
     * @param $email
     * @param $password
     * @return bool
     */

    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE email = :email AND password = :password';

        $result = $db->prepare($sql);

        $result->bindValue(':email', $email, PDO::PARAM_STR);
        $result->bindValue(':password', $password, PDO::PARAM_STR);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            return $user['id'];
        }

        return false;
    }

    /**
     * Save user in session
     * @param $userId
     */

    public static function saveUserInSession($userId)
    {
        session_start();
        $_SESSION['user'] = $userId;
    }

    /**
     * Check user is logged
     * @param $name
     * @return bool
     */

    public static function checkLogged()
    {
        session_start();

        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("Location: /user/login");
    }

    /**
     * Check user is guest
     * @param $name
     * @return bool
     */

    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    /**
     * Check name
     * @param $name
     * @return bool
     */

    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    /**
     * Check password
     * @param $password
     * @return bool
     */

    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    /**
     * Check email
     * @param $email
     * @return bool
     */

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    /**
     * Check email exits
     * @param $email
     * @return bool
     */

    public static function checkEmailExists($email)
    {

        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindValue(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }

    /**
     * List of users in the database
     * @return array
     */

    public static function getUserById($id)
    {
        if ($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindValue(':id', $id, PDO::PARAM_INT);


            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();


            return $result->fetch();
        }
    }

    /**
     * Insert image in the database
     * @return array
     */

    public static function insertImage($title, $image_url, $status, $position, $user_id, $link)
    {

        $db = Db::getConnection();

        $sql = 'INSERT INTO image (title, url, status, position, user_id, link) '
            . 'VALUES (:title, :url, :status, :position, :user_id, :link)';

        $result = $db->prepare($sql);
        $result->bindValue(':title', $title, PDO::PARAM_STR);
        $result->bindValue(':url', $image_url, PDO::PARAM_STR);
        $result->bindValue(':status', $status, PDO::PARAM_INT);
        $result->bindValue(':position', $position, PDO::PARAM_INT);
        $result->bindValue(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindValue(':link', $link, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * List of images in the database
     * @return array
     */

    public static function getImages($user_id)
    {

        $db = Db::getConnection();

        $Images = array();

        $result = $db->query('SELECT id, title, url, user_id, link, position FROM image' .
        ' WHERE user_id=' . $user_id . ' ORDER BY position ASC');

        $i = 0;
        while ($row = $result->fetch()) {
            $Images[$i]['id'] = $row['id'];
            $Images[$i]['title'] = $row['title'];
            $Images[$i]['url'] = $row['url'];
            $Images[$i]['user_id'] = $row['user_id'];
            $Images[$i]['link'] = $row['link'];
            $Images[$i]['position'] = $row['position'];
            $i++;
        }

        return $Images;
    }

    /**
     * Delete images in the database
     * @param $user_id
     * @return array
     */

    public static function deleteImages($user_id)
    {

        $db = Db::getConnection();

        $Images = array();

        $result = $db->query('SELECT id, title, url FROM image WHERE user_id=' . $user_id);

        $i = 0;
        while ($row = $result->fetch()) {
            $Images[$i]['id'] = $row['id'];
            $Images[$i]['title'] = $row['title'];
            $Images[$i]['url'] = $row['url'];
            $i++;
        }

        return $Images;
    }
}
