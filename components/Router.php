<?php

class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     *  Метод возращает строку запроса (что ввели в строке - то и возвращает)
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        $uri = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {


                /**
                 *  Для routes.php получаем внутренний путь из внешнего согласно правилу.
                 */
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);


                /**
                 *  Определяем имя контролллера и экшина которые будут обрабатывать запрос
                 */
                $segments = explode('/', $path);

                $controllerName = array_shift($segments) . 'Controller';


                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst((array_shift($segments)));


                $parameters = $segments;


                /**
                 *  Задаем имя файла контролллера (то, что мы получили выше) будут обрабатывать запрос
                 */
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                /**
                 *  Создаем объект с именем $controllerName и вызываем в нем функцию с именем
                 *  $actionName
                 */
                $controllerObject = new $controllerName;
                // $result = $controllerObject->$actionName();

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null) {
                    break;
                }
            }

        }
    }
}