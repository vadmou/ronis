<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Слайдер</title>

    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/jquery.flexslider-min.js"></script>
    <script src="/template/js/cabinet/slider.js"></script>
    <script src="/template/js/bootstrap.min.js"></script>

    <link type="text/css" href="/template/css/bootstrap.min.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/flexslider.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/style.css" rel="stylesheet"/>
</head>
<body>
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <h2>Слайдер</h2>

        <h4>Привет, <?php echo $user['name']; ?>!</h4>

        <a href="/cabinet" class="btn btn-primary btn-xs">Перейти в кабинет</a>
        <a href="/user/logout" class="btn btn-primary btn-xs">Сменить пользователя</a>
    </div>

    <div class="col-md-7">
        <div class="flexslider">
            <ul class="slides">
                <?php foreach ($Images as $image): ?>
                    <li>
                        <a href="<?= $image['link']; ?>"><img src="/<?php echo $image['url']; ?>"/><a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
</body>
</html>