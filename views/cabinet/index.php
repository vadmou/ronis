<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Кабинет</title>

    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/cabinet/index.js"></script>
    <script src="/template/js/jquery.validate.min.js"></script>
    <script src="/template/js/bootstrap.min.js"></script>

    <link type="text/css" href="/template/css/bootstrap.min.css" rel="stylesheet"/>
    <link type="text/css" href="/template/css/style.css" rel="stylesheet"/>

    <script>var userId =<?=$_SESSION['user'];?>;</script>
</head>
<body>
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <div    >
            <h2>Личный кабинет</h2>

            <h4>Привет, <?= $user['name']; ?>! Вы находитесь в личном кабинете.</h4>

            <strong>В данном кабинете можно настроить слайдер</strong><br>

            <form action="#" enctype="multipart/form-data" method="post">
                Добавить изображение<br>
                <input type="text" name="title" placeholder="Название изображения"/><br>
                <input type="text" name="link" placeholder="Ссылка, по умолчанию #"/><br>
                <input type="text" name="position" placeholder="Позиция"/><br>
                <input type="file" name="image"/><br>
                <input type="submit" name="submit" value="Внести"/>
            </form>
            <div>
                <a href="/cabinet/slider" class="btn btn-primary btn-xs">Перейти к слайдеру</a>
                <a href="/user/logout" class="btn btn-primary btn-xs">Сменить пользователя</a>
            </div>
        </div>
    </div>

    <div class="col-md-7" id="notes">
        <?php foreach ($Images as $image): ?>
            <div>
                Название: <?= $image['title']; ?><br>
                Позиция в списке: <input id="position" type="text" name="position"
                                         value="<?= $image['position']; ?>"/>
                <button name="button" class="changePosition" value="<?= $image['id']; ?>">Изменить</button>
                <br>
                <a href="<?= $image['link']; ?>"><img class="del" src="/<?= $image['url']; ?>"></a>
                <button name="button" class="del btn btn-danger" value="<?= $image['id']; ?>">Удалить</button>
                <hr>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</body>
</html>