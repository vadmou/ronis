<?php
error_reporting(-1);
ini_set('display_errors', false);
ini_set('log_errors', true);
ini_set('error_log', 'error.log');

// FRONT COTROLLER

define('ROOT', dirname(__FILE__));
require_once(ROOT . '/components/Router.php');
require_once(ROOT . '/components/Db.php');

$router = new Router();
$router->run();