<?php

return array(

    'user/logout' => 'user/logout',
    'cabinet/slider' => 'cabinet/slider',
    'cabinet' => 'cabinet/index',
    'register' => 'user/register',
    '' => 'user/login',
);