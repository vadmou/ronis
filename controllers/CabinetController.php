<?php

include_once ROOT . '/models/User.php';
include_once ROOT . '/modules/Image.php';

class CabinetController
{

    public function actionIndex()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $user_id = $user['id'];

        $title = '';
        $position = '';
        $link = '';

        if (isset($_POST['submit']) && isset($_FILES)) {

            $title = htmlspecialchars($_POST['title'], ENT_QUOTES);

            $status = 1;

            $position = htmlspecialchars($_POST['position'], ENT_QUOTES);
            $link = htmlspecialchars($_POST['link'], ENT_QUOTES);

            move_uploaded_file($_FILES['image']['tmp_name'], 'users/image/' . $_FILES['image']['name']);
            $url = 'users/image/' . $_FILES['image']['name'];

            $image_url = Image::resize($url);

            User::insertImage($title, $image_url, $status, $position, $user_id, $link);
        }

        $Images = '';
        $Images = User::getImages($user_id);


        require_once(ROOT . '/views/cabinet/index.php');

        return true;
    }

    public function actionSlider()
    {

        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $user_id = $user['id'];

        $Images = '';
        $Images = User::getImages($user_id);

        require_once(ROOT . '/views/cabinet/slider.php');

        return true;
    }
}